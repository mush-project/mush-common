use crate::signals::client_signal::{ClientSignal, CLIENT_SIGNAL_MAGIC};

/// A wrapper around the ClientSignal useful to verify the integrity of the Signal
pub struct ClientData {
    pub signal: ClientSignal,
}

impl ClientData {
    /// Returns a new ClientData, but does a magic number checking before returning anything
    pub fn new(magic: &[u8; 4], signal: ClientSignal) -> Result<ClientData, String> {
        if magic != CLIENT_SIGNAL_MAGIC {
            Err(format!("Bad magic"))
        } else {
            Ok(ClientData { signal })
        }
    }
}
