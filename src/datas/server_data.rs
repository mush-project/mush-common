use crate::signals::server_signal::{ServerSignal, SERVER_SIGNAL_MAGIC};

/// A wrapper around the ServerSignal useful to verify the integrity of the Signal
pub struct ServerData {
    pub signal: ServerSignal,
}

impl<'a> ServerData {
    /// Returns a new ServerData, but does a magic number checking before returning anything
    pub fn new(magic: &[u8; 4], signal: ServerSignal) -> Result<ServerData, String> {
        if magic != SERVER_SIGNAL_MAGIC {
            Err(format!("Bad magic"))
        } else {
            Ok(ServerData { signal })
        }
    }
}
