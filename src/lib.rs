pub mod datas;
pub mod signals;

#[cfg(test)]
pub mod tests;

/// Type definition
pub type Id = u32;
