use crate::Id;
use serde_derive::{Deserialize, Serialize};

// Before any client signal this buffer is sent to indicate a future client signal
pub const CLIENT_SIGNAL_MAGIC: &[u8; 4] = &[42, 52, 64, 37];

/// Client signals used to communicate with the Server
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum ClientSignal {
    // Session start/end to know when the connection starts or end.
    SessionStart,
    SessionEnd,

    // ... Pong !
    Ping,

    // Keep alive signal, in response of the server's request
    KeepAlive,

    // If server needs a password
    ConnectWith(Vec<u8>),

    // Requests a streaming of the specified ID
    StreamId(Id),

    // Requests a streaming of a random ID
    // We let the server deciding wich item it gonna stream because idk lol
    StreamRandom,

    // Requests the list of all the available server's items
    GetAll,
}

impl<'a> super::SignalSerde<'a> for ClientSignal {}

impl ClientSignal {
    // Prepares the buffer to send to the other side.
    pub fn prepare_send(&self) -> Result<Vec<u8>, String> {
        let serialized_sig = bincode::serialize(&self);

        match serialized_sig {
            Ok(mut bin) => {
                let mut to_ret = Vec::<u8>::new();

                let mut magic_vec = CLIENT_SIGNAL_MAGIC.to_vec();
                to_ret.append(&mut magic_vec);
                to_ret.push(to_ret.len() as u8);
                to_ret.append(&mut bin);

                Ok(to_ret)
            }
            Err(e) => Err(format!(
                "Cannot serialize the signal {:?}. \nError: {:?}",
                &self, e
            )),
        }
    }
}
