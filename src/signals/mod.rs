pub mod client_signal;
pub mod server_signal;

use serde::{Deserialize, Serialize};
use crate::datas::{client_data::ClientData, server_data::ServerData};
use crate::signals::{server_signal::SERVER_SIGNAL_MAGIC, client_signal::{CLIENT_SIGNAL_MAGIC}};

/// Common trait shared between ClientSignal and ServerSignal.
pub trait SignalSerde<'de>
where
    Self: Serialize + Deserialize<'de> + std::fmt::Debug,
{   
    /// Serializes the Signal into a u8's vector ready to be sent to the other side
    fn serialize_this(&self) -> Result<Vec<u8>, String> {
        match bincode::serialize(&self) {
            Ok(bin) => Ok(bin),
            Err(e) => Err(format!(
                "Cannot serialize a signal: {:?}\nError: {:?}",
                &self, e
            )),
        }
    }
}

pub fn deserialize_buf<'a, S>(buf: &'a Vec<u8>) -> Result<S, String> 
where S: SignalSerde<'a> {
    match bincode::deserialize::<S>(buf.as_slice()) {
        Ok(s) => Ok(s),
        Err(e) => Err(format!("Cannot deserialize a signal: {:?}", e)),
    }
}

// Checks all the buffer in search of eventual client datas
pub fn search_client_datas(buf: &[u8; 1024], limit: usize) -> Vec<ClientData> {
    let mut to_ret = Vec::<ClientData>::new();
    
    for magic_index in search_magics(&buf, limit, CLIENT_SIGNAL_MAGIC) {
        let magic = [buf[magic_index - 4], buf[magic_index - 3], buf[magic_index - 2], buf[magic_index - 1]];
        let sig_size = buf[magic_index] as usize;
        let sig = deserialize_buf::<crate::signals::client_signal::ClientSignal>(&split(buf[magic_index + 1], buf[magic_index + sig_size + 1], &buf));
        
        if let Ok(s) = sig {
            // TODO: Should be better
            to_ret.push(ClientData::new(&magic, s).unwrap());
        }
        else {
            continue;
        }
    }
    to_ret
} 

// Checks all the buffer in search of eventual server datas
pub fn search_server_datas(buf: &[u8; 1024], limit: usize) -> Vec<ServerData> {
    let mut to_ret = Vec::<ServerData>::new();
    
    for magic_index in search_magics(&buf, limit, SERVER_SIGNAL_MAGIC) {
        let magic = [buf[magic_index - 4], buf[magic_index - 3], buf[magic_index - 2], buf[magic_index - 1]];
        let sig_size = buf[magic_index] as usize;
        let sig = deserialize_buf::<crate::signals::server_signal::ServerSignal>(&split(buf[magic_index + 1], buf[magic_index + sig_size + 1], &buf));
        
        if let Ok(s) = sig {
            // TODO: Should be better
            to_ret.push(ServerData::new(&magic, s).unwrap());
        }
        else {
            continue;
        }
    }
    to_ret
} 

// Lists all the index where there was a client magic before
fn search_magics(buf: &[u8; 1024], limit: usize, magic: &[u8; 4]) -> Vec<usize> {
    let mut magic_index = 0usize;
    let mut buf_index = 0usize;

    let mut magics = Vec::<usize>::new();
    
    for i in 0..limit {
        buf_index += 1;
        if buf[i] == magic[magic_index] {
            magic_index += 1;
        }

        if magic_index > 3 {
            magics.push(buf_index);
            magic_index = 0;
            buf_index -= 1;
        }
    }
    magics
}

fn split(start: u8, end: u8, slice: &[u8; 1024]) -> Vec<u8> {
    let mut to_ret = Vec::<u8>::new();

    for i in start..=end {
        to_ret.push(slice[i as usize]);
    }
    to_ret
}