use crate::Id;
use serde_derive::{Deserialize, Serialize};
// Before any server signal this buffer is sent to indicate a future server signal
pub const SERVER_SIGNAL_MAGIC: &[u8; 4] = &[12, 73, 42, 51];

/// Server signals used to communicate with the Client
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum ServerSignal {
    // Connection Signals
    ConnectionAccepted,
    ConnectionRefused(String),
    ConnectionAbort(String),

    // Ping...
    Pong,

    // Password asking if configured as this
    RequestPassword,

    // Requires a keep alive from the client
    KeepAliveRequired,

    // Start and end of the stream, with the associated Id and the total size of the buffer that is gonna be streamed
    StreamStart(Id, usize),
    StreamEnd(Id),
}

impl<'a> super::SignalSerde<'a> for ServerSignal {}

impl ServerSignal {
    /// Prepares the buffer to send to the other side.
    pub fn prepare_send(&self) -> Result<Vec<u8>, String> {
        let serialized_sig = bincode::serialize(&self);

        match serialized_sig {
            Ok(mut bin) => {
                let mut to_ret = Vec::<u8>::new();
                let mut magic_vec = SERVER_SIGNAL_MAGIC.to_vec();
                
                to_ret.append(&mut magic_vec);
                to_ret.push(to_ret.len() as u8);
                to_ret.append(&mut bin);

                Ok(to_ret)
            }
            Err(e) => Err(format!(
                "Cannot serialize the signal {:?}. \nError: {:?}",
                &self, e
            )),
        }
    }
}
