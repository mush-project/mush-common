use crate::signals::client_signal::{ClientSignal};
use crate::signals::server_signal::{ServerSignal};
use crate::signals::deserialize_buf;

#[test]
fn signals_serde_integrity_test() {
    let client_signal = ClientSignal::Ping;
    let server_signal = ServerSignal::Pong;

    let ser_client = client_signal.prepare_send().unwrap();
    let ser_server = server_signal.prepare_send().unwrap();

    if let Ok(cli_sig) = deserialize_buf::<ClientSignal>(&ser_client) {
        assert_eq!(cli_sig, client_signal);
    }

    if let Ok(ser_sig) = deserialize_buf::<ServerSignal>(&ser_server) {
        assert_eq!(ser_sig, server_signal);
    }
}

#[test]
fn signal_serde_correct() {
    let client_signal = ClientSignal::Ping;
    let server_signal = ServerSignal::Pong;
    
    client_signal.prepare_send().unwrap();
    server_signal.prepare_send().unwrap();
}

#[test]
#[should_panic]
fn signal_serde_not_different_before_serde_and_after() {
    let ser_sig = ClientSignal::StreamId(35);
    let wrong_sig = ClientSignal::Ping;
    
    let ser = ser_sig.prepare_send().unwrap();
    assert_eq!(deserialize_buf::<ClientSignal>(&ser).unwrap(), wrong_sig);
}